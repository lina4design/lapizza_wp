<?php
get_header();
while (have_posts()): the_post()
    ?>
	<div class="hero" style="background-image:url(<?php echo get_the_post_thumbnail_url() ?>)">
		<div class="hero-content">
			<div class="hero-text">
				<h2>
					<?php the_title()?>
				</h2>
			</div>
		</div>
	</div>
	<div class="main-content container clear">
		<div class="content-text">
				<?php the_content()?>
		</div>
	</div>
<?php
endwhile;
get_footer()
?>