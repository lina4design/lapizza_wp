<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>La Pizza</title>
    <?php
// to make attachments
wp_head();
?>
</head>

<body <?php body_class();?>>
    <header class="site-header">
        <div class="container">
            <div class="logo ">
                <a href="<?php echo esc_url(home_url()) ?>">
                    <img src="<?php echo get_template_directory_uri() ?>/img/logo.svg" alt="logo" class=logo-image>
                </a>
            </div>
            <div class="contact-info">
                <div class="socials">
                    <?php
$args = [
    'theme_location' => 'social-menu',
    'container' => 'nav',
    'container_class' => 'social-links',
    'container_id' => 'socials',
    'link_before' => '<span class="sr-text">',
    'link_after' => '</span>',
];
wp_nav_menu($args);
?>
                </div>
                <div class="address">
                    <small>
                        <p><?php echo esc_html(get_option('location'))?></p>
                        <p>Phone: <?php echo esc_html(get_option('phone'))?></p>
                    </small>
                </div>
            </div>
        </div>
    </header>
    <div class="main-menu">
        <div class="mobile-menu">
            <a href="#" class="mobile">
                <i class="fa fa-bars"></i> Menu</a>
        </div>
        <div class="navigation container">
            <?php
$args = [
    'theme_location' => 'header-menu',
    'container' => 'nav',
    'container_class' => 'site-nav',
];
wp_nav_menu($args);
?>
        </div>
    </div>