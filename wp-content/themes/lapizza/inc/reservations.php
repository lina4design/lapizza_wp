<?php
function lapizza_save_reservation()
{

    global $wpdb;

    if (isset($_POST['reservation']) && $_POST['hidden'] == "1") {
        $name = sanitize_text_field($_POST['name']);
        $date = sanitize_text_field($_POST['date']);
        $email = sanitize_email($_POST['email']);
        $phone = sanitize_text_field($_POST['phone']);
        $message = sanitize_textarea_field($_POST['message']);

        $table = $wpdb->prefix . 'reservations';
        // database row name => POST value
        $data = [
            'name' => $name,
            'date' => $date,
            'email' => $email,
            'phone' => $phone,
            'message' => $message,
        ];

        $format = [
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
        ];
        // insert data into CUSTOM database tables
        $wpdb->insert($table, $data, $format);

        $url = get_page_by_title('Thanks for your reservation!');
        wp_redirect(get_permalink($url));
        // need to add exit for redirection
        exit();
    }
}
add_action('init', 'lapizza_save_reservation');
