<?php
function lapizza_database()
{
    // adds more WP functionalities
    global $wpdb;

    global $lapizza_db_veraion;

    $lapizza_db_veraion = "1.0";

    // $wpdb->prefix in case the $table_prefix in wp_config.php file has been changed
    $table = $wpdb->prefix . "reservations";
    $charset_collate = $wpdb->get_charset_collate();

    // SQL statement
    $sql = "CREATE TABLE $table (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            name varchar(50) NOT NULL,
            date datetime NOT NULL,
            email varchar(50) DEFAULT '' NOT NULL,
            phone varchar(10) NOT NULL,
            message longtext NOT NULL,
            PRIMARY KEY (id)
    ) $charset_collate; ";

    // always add require_once when using $wpdb or creating a table
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
}
add_action('after_setup_theme', 'lapizza_database')
?>
