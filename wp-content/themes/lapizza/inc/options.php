<?php
function lapizza_options()
{
    add_menu_page('La Pizza', 'La Pizza Options', 'administrator', 'lapizza_options',
        'lapizza_adjustments', '', 20);
    add_submenu_page('lapizza_options', 'Reservations', 'Reservations', 'administrator',
        'lapizza_reservations', 'lapizza_reservations');
}
add_action('admin_menu', 'lapizza_options');

function lapizza_settings (){
    // Information group
    register_setting('lapizza_setting_info', 'location');
    register_setting('lapizza_setting_info', 'phone');

    // Google Maps Group
    register_setting('lapizza_settings_gmap', 'gmap_latitude');
    register_setting('lapizza_settings_gmap', 'gmap_longitude');
    register_setting('lapizza_settings_gmap', 'gmap_zoom');
    register_setting('lapizza_settings_gmap', 'gmap_apikey');

}

add_action('init','lapizza_settings');

function lapizza_adjustments(){ ?>
    <div class="wrap">
        <h1>
        Lapizza Adjustments
        </h1>
        <form action="options.php" method="post">
            <?php 
                settings_fields('lapizza_settings_gmap');
                do_settings_sections('lapizza_settings_gmap')
            ?>
            <h2>
            Google Maps
            </h2>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Latitude: </th>
                    <td>
                        <input type="number" step="0.00000001" name="gmap_latitude" value="<?php echo esc_attr( get_option('gmap_latitude')) ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Longitude: </th>
                    <td>
                        <input type="number" name="gmap_longitude" step="0.00000001" value="<?php echo esc_attr( get_option('gmap_longitude')) ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Zoom level: </th>
                    <td>
                        <input type="number" name="gmap_zoom" step="1" min="2" max="21" value="<?php echo esc_attr( get_option('gmap_zoom')) ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">API key: </th>
                    <td>
                        <input type="text" name="gmap_apikey" value="<?php echo esc_attr( get_option('gmap_apikey')) ?>">
                    </td>
                </tr>
            </table>
            <?php 
                settings_fields('lapizza_setting_info');
                do_settings_sections('lapizza_setting_info')
            ?>
            <h2>
            Contact Information
            </h2>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Address: </th>
                    <td>
                        <input type="text" name="location" value="<?php echo esc_attr( get_option('location')) ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Phone number: </th>
                    <td>
                        <input type="tel" name="phone" step="0.00000001" value="<?php echo esc_attr( get_option('phone')) ?>">
                    </td>
                </tr>
            </table>
            <?php submit_button() ?>
        </form>
    </div>

<?php
 }
function lapizza_reservations()
{?>
    <div class="wrap">
        <h1>Reservations</h1>
        <table class="wp-list-table widefat striped">
            <thead>
                <tr>
                    <th class="manage-column">ID</th>
                    <th class="manage-column">Name</th>
                    <th class="manage-column">Date of reservation</th>
                    <th class="manage-column">E-mail</th>
                    <th class="manage-column">Phone number</th>
                    <th class="manage-column">Message </th>
                </tr>
            </thead>
            <tbody>
            <?php
                global $wpdb;
                $table = $wpdb->prefix . 'reservations';
                $reservations = $wpdb->get_results("SELECT * FROM $table", ARRAY_A);
                foreach($reservations as $reservation): ?>
                    <tr>
                        <td><?php echo $reservation['id']?></td>
                        <td><?php echo $reservation['name']?></td>
                        <td><?php echo $reservation['date']?></td>
                        <td><?php echo $reservation['email']?></td>
                        <td><?php echo $reservation['phone']?></td>
                        <td><?php echo $reservation['message']?></td>

                    </tr>


            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

<?php }?>
