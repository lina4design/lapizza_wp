<?php
get_header();
while (have_posts()): the_post()
    ?>
		    <div class="hero" style="background-image:url(<?php echo get_the_post_thumbnail_url() ?>)">
		        <div class="hero-content">
		            <div class="hero-text">
		                <h2>
		                    <?php the_title()?>
		                </h2>
		            </div>
		        </div>
		    </div>
		    <div class="main-content container">
		        <div class="content-text">
		            <?php the_content()?>
		        </div>
		    </div>
		    <div class="box-information container clear">
		        <div class="box">
		            <?php
    $id_image = get_field('image_1');
    $image = wp_get_attachment_image_src($id_image, 'boxes');
    ?>
		            <img class="box-image" src="<?php echo $image[0] ?>" alt="">
	                <div class="content-box">
		            <?php the_field('description_1')?>
		            </div>
		        </div>
		        <!-- Second column -->
		        <div class="box">
		            <div class="content-box">
		                <?php the_field('description_2')?>
		            </div>
		                <?php
    $id_image = get_field('image_2');
    $image = wp_get_attachment_image_src($id_image, 'boxes');
    ?>
	    <img class="box-image" src="<?php echo $image[0] ?>" alt="">
		        </div>
		        <!-- third column-->
		        <div class="box">
		            <?php
    $id_image = get_field('image_3');
    $image = wp_get_attachment_image_src($id_image, 'boxes');
    ?>
		            <img class="box-image" src="<?php echo $image[0] ?>" alt="">
		            <div class="content-box">
		                <?php the_field('description_3')?>
		            </div>
		        </div>
		    </div>
		    <?php
endwhile;
get_footer()
?>