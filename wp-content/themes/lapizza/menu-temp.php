<?php
/**
 * Template name: Menu template
 */
get_header();
while (have_posts()): the_post()?>
		<div class="hero" style="background-image:url(<?php echo get_the_post_thumbnail_url() ?>)">
			<div class="hero-content">
				<div class="hero-text">
					<h2>
						<?php the_title()?>
					</h2>
				</div>
			</div>
		</div>
		<div class="main-content container">
			<div class="content-text">
				<?php the_content()?>
			</div>
		</div>
	<?php endwhile?>

<div class="our-specialties container">
<!-- Pizzas -->
	<h3 class="primary-text">
		Pizzas
	</h3>
	<div class="container-grid">
	<?php
$args = [
    'post_type' => 'specialties',
    'post_per_page' => 10,
    'orderby' => 'title',
    'order' => 'ASC',
    'category_name' => 'pizzas',
];
$pizzas = new WP_Query($args);
while ($pizzas->have_posts()): $pizzas->the_post(); // -> access elements of an object
    ?>
			<div class="columns2-4 menu-specialty">
				<a href="<?php the_permalink()?>">
					<?php the_post_thumbnail('specialties')?>
					<h4><?php the_title()?><span> $ <?php the_field('price')?></span></h4>
					<?php the_content()?>
				</a>
			</div>

		<?php endwhile;
wp_reset_postdata()?>
	</div>
<!-- Others -->
	<h3 class="primary-text">
		Others
	</h3>
	<div class="container-grid">
	<?php
$args = [
    'post_type' => 'specialties',
    'post_per_page' => 10,
    'orderby' => 'title',
    'order' => 'ASC',
    'category_name' => 'others',
];
$others = new WP_Query($args);
while ($others->have_posts()): $others->the_post(); // -> access elements of an object
    ?>
			<div class="columns2-4 menu-specialty">
				<a href="<?php the_permalink()?>">
					<?php the_post_thumbnail('specialties')?>
					<h4><?php the_title()?><span> $ <?php the_field('price')?></span></h4>
					<?php the_content()?>
				</a>
			</div>

		<?php endwhile;
wp_reset_postdata()?>
	</div>
</div>
<?php get_footer();?>