<?php
get_header();
while (have_posts()): the_post()
    ?>
				<div class="hero" style="background-image:url(<?php echo get_the_post_thumbnail_url() ?>)">
					<div class="hero-content">
						<div class="hero-text">
							<h2>
								<?php the_title()?>
							</h2>
						</div>
					</div>
				</div>
				<div class="main-content container">
					<div class="content-text">
	                    <p class="ingredients">Ingredients:</p>
	                    <?php the_content()?>
	                    <p class="price"><span> Price: $ <?php the_field('price')?></span></p>
					</div>
				</div>
			<?php
endwhile;
get_footer()
?>