<?php

// linking or importing the database.php file

require get_template_directory() . '/inc/database.php';
require get_template_directory() . '/inc/reservations.php';
require get_template_directory() . '/inc/options.php';

function lapizza_setup()
{
    add_theme_support('post-thumbnails');

    add_image_size('boxes', 437, 291, true);
    add_image_size('specialties', 768, 515, true);
    add_image_size('specialty-portrait', 435, 530, true);


    update_option('thumbnail_size_w', 253);
    update_option('thumbnail_size_h', 164);

}
add_action('after_setup_theme', 'lapizza_setup');
// adds all styles to theme lapizza
function lapizza_styles()
{
    // where is my style file? If I have more, I can put others as an array
    // register style
    // wp_register_style('bootstrap_css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', []);
    wp_register_style('datetime-local', get_template_directory_uri() . '/css/datetime-local-polyfill.css', [], '');
    wp_register_style('google_font', 'https://fonts.googleapis.com/css?family=Open+Sans:400,700|Raleway:400,700,900', [], '1.0.0');
    wp_register_style('normalize', get_template_directory_uri() . '/css/normalize.css', [], '8.0.0');
    wp_register_style('fluidbox', get_template_directory_uri() . '/css/fluidbox.min.css', [], '4.9.8');
    wp_register_style('fontawesome', get_template_directory_uri() . '/css/all.min.css', [], '5.2.0');
    wp_register_style('style', get_template_directory_uri() . '/css/style.css', ['normalize'], '4.0');

    // using it
    //  wp_enqueue_style('bootstrap_css');
    wp_enqueue_style('google_font');
    wp_enqueue_style('normalize');
    wp_enqueue_style('fluidbox');
    wp_enqueue_style('fontawesome');
    wp_enqueue_style('style');
}

// hook
add_action('wp_enqueue_scripts', 'lapizza_styles');

// adds menus
function lapizza_menus()
{
    // adds 'Menus' to WordPress Appearance
    register_nav_menus(
        [
            'header-menu' => __('Header Menu', 'lapizza'),
            'social-menu' => __('Social Menu', 'lapizza'),
        ]
    );
}
add_action('init', 'lapizza_menus');


function lapizza_js()
{
    $apikey = esc_html(get_option('gmap_apikey'));
    global $wp_scripts;
    wp_register_script('googlemaps', 'https://maps.googleapis.com/maps/api/js?key='.$apikey.'&callback=initMap', [], '', true);    
    wp_register_style('script_js', get_template_directory_uri() . '/js/main.js',[], '1.0', true);
    wp_register_script('jquery', 'https://code.jquery.com/jquery-3.3.1.slim.min.js', [], '3.3.1', true);    
    wp_register_script('modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', ['jquery'], '2.8.3', true);    
    wp_register_script('datetime-local-polyfill', get_template_directory_uri() . '/js/datetime-local-polyfill.min.js',['jquery ', 'jquery-ui-core','jquery-ui-datepicker'], '1.0', true);
    
    
    wp_enqueue_script('jquery');
    wp_enqueue_script('popper_js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js');
    wp_enqueue_script('bootstrap_js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js');
    wp_enqueue_script('ajax_js', 'http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js');
    wp_enqueue_script('debounce_js', get_template_directory_uri() . '/js/debounce.js');
    wp_enqueue_script('fluidbox_js', get_template_directory_uri() . '/js/jquery.fluidbox.min.js');
    wp_enqueue_script('googlemaps');
    wp_enqueue_script('jquery-ui-core');
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_script('datetime-local-polyfill');
    wp_enqueue_script('modernizr');
    wp_enqueue_script('script_js', get_template_directory_uri() . '/js/main.js');

    // for google map

    wp_localize_script( 
        'script_js', 'options', [
            'latitude'  => esc_html(get_option('gmap_latitude')),
            'longitude' => esc_html(get_option('gmap_longitude')),
            'zoom'      => esc_html(get_option('gmap_zoom')),
        ] 
    );

}

add_action('wp_enqueue_scripts', 'lapizza_js');

function lapizza_specialties()
{
    $labels = [
        'name' => _x('Pizzas', 'lapizza'),
        'singular_name' => _x('Pizza', 'post type singular name', 'lapizza'),
        'menu_name' => _x('Pizzas', 'admin menu', 'lapizza'),
        'name_admin_bar' => _x('Pizzas', 'add new on admin bar', 'lapizza'),
        'add_new' => _x('Add New', 'book', 'lapizza'),
        'add_new_item' => __('Add New Pizza', 'lapizza'),
        'new_item' => __('New Pizza', 'lapizza'),
        'edit_item' => __('Edit Pizzas', 'lapizza'),
        'view_item' => __('View Pizzas', 'lapizza'),
        'all_items' => __('All Pizzas', 'lapizza'),
        'search_items' => __('Search Pizzas', 'lapizza'),
        'parent_item_colon' => __('Parent Pizzas', 'lapizza'),
        'not_found' => __('No Pizzases found.', 'lapizza'),
        'not_found_in_trash' => __('No Pizzases found in Trash', 'lapizza'),
    ];

    $args = [
        'labels' => $labels,
        'description' => __('Description', 'lapizza'),
        'public' => true,
        'publicly_queryble' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => ['slug' => 'specialties'],
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 6,
        'supports' => ['title', 'editor', 'thumbnail'],
        'taxonomies' => ['category'],
    ];

    register_post_type('specialties', $args);
}
add_action('init', 'lapizza_specialties');

/* widgets */

function lapizza_widgets()
{
    register_sidebar(
        [
            'name' => 'Blog Sidebar',
            'id' => 'blog_sidebar',
            'before_widget' => '<div class="widget">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>',

        ]
    );
}
add_action('widgets_init', 'lapizza_widgets');

function add_async_defer($tag, $handle){
    if('googlemap_js' !== $handle){
        return $tag;
    }
    return str_replace('src', 'async="async" defer="defer" src', $tag);
}
add_filter('script_loader_tag','add_async_defer', 10, 2);