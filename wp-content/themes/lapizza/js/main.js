var map;

function initMap() {
  var location = {
    lat: parseFloat(options.latitude),
    lng: parseFloat(options.longitude),
  }
  map = new google.maps.Map(document.getElementById('map'), {
    center: location,
    zoom: parseInt(options.zoom),
  });

  var marker = new google.maps.Marker({
    position: location,
    map: map,
    title: 'La Pizza',
  });

}

$(document).ready(function () {
  // menu button
  $(".mobile-menu a").on("click", function () {
    $("nav.site-nav").toggle("slow");
  });
  // show the mobile menu
  let breakpoint = 768;
  $(window).resize(function () {
    boxAdjustment();
    if ($(document).width() >= breakpoint) {
      $("nav.site-nav").show();
    } else {
      $("nav.site-nav").hide();
    }
  });
  boxAdjustment();

  // fluidbox

  jQuery('.gallery a').each(function () {
    jQuery(this).attr({
      'data-fluidbox': ''
    });

    if (jQuery('[data-fluidbox]').length > 0) {
      jQuery('[data-fluidbox]').fluidbox()
    }
  })

});

function boxAdjustment() {
  // adapt box images
  var images = $('.box-image');
  if (images.length > 0) {
    var imageHeight = images[0].height;
    var boxes = $('.content-box');
    $(boxes).each(function (i, element) {
      $(element).css({
        'height': imageHeight + 'px'
      })
    })
  }
}

function displayMap(value) {
  if (value == 0) {
    let locationSection = $(".location-reservation");
    let locationHeight = locationSection.height();
    $('#map').css({
      'height': locationHeight
    });
  } else {
    $('#map').css({
      'height': value
    })

  }
}