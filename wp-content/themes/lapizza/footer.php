<footer>
    <?php
    $args = [
    'theme_location' => 'header-menu',
    'container' => 'nav',
    'after' => '<span class="separator"> | </span>',
    ];
    wp_nav_menu($args);
    ?>
    <div class="location">
        <p><?php echo esc_html(get_option('location'))?></p>
        <p>Phone: <?php echo esc_html(get_option('phone'))?></p>
    </div>
</footer>
<?php
wp_footer();
?>
    </body>
</html>
