<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lapizza_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'KZLfr-}_{qTD]`nbAspA}O`;F(;j89cpN5]/hWE3gH{--gNMwf-cDILjOVhZv*z<');
define('SECURE_AUTH_KEY',  'qDZ3QxG:b`IKxhRY{q,N-.7xrp{Ygr5P<RrSM kJwFx^yP<*OZ >71fxIEzi 8@q');
define('LOGGED_IN_KEY',    '>GG>}:j7-iyWN>SFe%l_0NN<_xbiGbFmv7kAp4|`ecuH~`U.bfnPbo`t{?M`+sgm');
define('NONCE_KEY',        't9o,6+-S&)3&jL96r|Z8_IeDYOgiL5!>zf1<>/%drI2cr_6A>Q`8%.zdGl`tf&^V');
define('AUTH_SALT',        '6`drQ!EV7Wv*#f5?W^S/Q,q[XK5L0Ww8x:>_G<x?U}b4xa{KLKe^Y9zDatP6%~RU');
define('SECURE_AUTH_SALT', 'C=bL/3WiXpTnU3*OsOkNd)&mb^QD,udZhL&_<IXZWHV?%eNw1JP3.Ojvkj8fVt0)');
define('LOGGED_IN_SALT',   ' d(hvh(nx/#Km^nZM6*YiQ6&P@RD(54_2G<(>nQC?]VJDEXi4YQv8A@?ZyR(G R1');
define('NONCE_SALT',       '3]8D!wh_?|P7<b5IeR:=6|^1<>kiaR9+Wel2[=en%3[b1zkn;$efhSp@j]3F.tYh');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
